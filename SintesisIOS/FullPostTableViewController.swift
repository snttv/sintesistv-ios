//
//  FullPostTableViewController.swift
//  SintesisIOS
//
//  Created by cesar on 4/25/18.
//  Copyright © 2018 sintesis. All rights reserved.
//

import UIKit
class FullPostTableViewController: UITableViewController,UIWebViewDelegate {
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var txt_title: UILabel!
    @IBOutlet weak var txt_date: UILabel!
    @IBOutlet weak var txt_autor: UILabel!
    @IBOutlet weak var webview_height: NSLayoutConstraint!
    @IBOutlet weak var txt_cat: UILabel!
    
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var webviewCell: UITableViewCell!
    @IBOutlet weak var header_cell: UITableViewCell!
    
    var postEmbed:PostEmbed? = nil
    var preImg:UIImage?
    let postServ = PostService()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 358
        tableView.rowHeight = UITableViewAutomaticDimension
        
        webview.scrollView.isScrollEnabled = false
        if let postEmbed = postEmbed {
            txt_title.text = postEmbed.title.rendered
                .replacingOccurrences(of: "&#8216;", with: "’")
                .replacingOccurrences(of: "&#8217;", with: "’")
                .replacingOccurrences(of: "&#8220;", with: "“")
                .replacingOccurrences(of: "&#8221;", with: "”")
                .replacingOccurrences(of: "&#8230;", with: "…")
            txt_date.text = postEmbed.dateFormat()
            txt_cat.text = postEmbed.category?.name ?? "Por síntesis"
            self.webview.delegate = self
            if(preImg != nil){
                headerImage.image=preImg
            }
            getJsonObject(urlStr: postEmbed.links.author[0].href, ok: {
                (author:AuthorModel) in
                DispatchQueue.main.async {
                    self.txt_autor.text = author.name
                }
            },errorFn:nil)
            
            getJsonObject(urlStr: "http://sintesistv.com.mx/wp-json/wp/v2/media/\(postEmbed.featuredMedia)", ok: { (photoJson:PhotoModel) in
                get(urlStr: photoJson.guid.rendered, ok: { data in
                    DispatchQueue.main.async {
                        self.headerImage.image = UIImage(data: data)
                    }
                },errorFn:nil)
            },errorFn:nil)
            var query = PostQuery()
            query.id = postEmbed.id
            postServ.fullPostWithQuery(query: query, ok: {
                (list:PostList) in
                DispatchQueue.main.async {
                    self.webview.loadHTMLString(self.htmlTemplate+list[0].content.rendered+self.htmlTemplate2, baseURL: nil)
                }
                    
                }, errorFn:{
                    _ in
                    let alert = UIAlertController(title: "Error al cargar la noticia", message:
                        "intentelo mas tarde", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
                    self.present(alert, animated: true, completion: nil)
                })
        }
    }
     func webViewDidFinishLoad(_ webView: UIWebView) {
        webview_height.constant = webview.scrollView.contentSize.height
        tableView.reloadData()
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row==0) ? super.tableView(tableView,heightForRowAt:indexPath) :
        webview.scrollView.contentSize.height
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print("\(navigationType.rawValue) \(request.mainDocumentURL! ) \(request.url!)" )
        
        
        if navigationType == UIWebViewNavigationType.linkClicked ||
            (!request.mainDocumentURL!.absoluteString.contains("about:blank") &&
                !request.url!.absoluteString.contains("about:blank") &&
                !request.mainDocumentURL!.absoluteString.contains("applewebdata") &&
                !request.url!.absoluteString.contains("applewebdata"))
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(request.url!, options: [:])
            } else {
                UIApplication.shared.openURL(request.url!)
            }
            return false
        }
        return true
        
    }
    
    
    let htmlTemplate = """
    <!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        img {
            width: 100%;
            height: auto;
        }
         iframe,object,embed {
            width: 100%;
            height: auto;
        }

    </style>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
<div class="container">
"""
    let htmlTemplate2="""
</div>
</body>
</html>
"""
}

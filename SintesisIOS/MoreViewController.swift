//
//  MoreViewController.swift
//  SintesisIOS
//
//  Created by cesar on 5/8/18.
//  Copyright © 2018 sintesis. All rights reserved.
//

import UIKit
import WebKit
import MessageUI
class MoreViewController: UIViewController,UITextViewDelegate,UITextFieldDelegate, UIWebViewDelegate, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var txt_page: UILabel!
    @IBOutlet weak var img_page: UIImageView!
    @IBOutlet weak var txt_twitter: UILabel!
    @IBOutlet weak var img_twitter: UIImageView!
    @IBOutlet weak var txt_facebook: UILabel!
    @IBOutlet weak var img_facebook: UIImageView!
    @IBOutlet weak var txt_youtube: UILabel!
    @IBOutlet weak var img_youtube: UIImageView!
    
    @IBOutlet weak var etxt_name: UITextField!
    //etx_subject
    @IBOutlet weak var etxt_email: UITextField!
    @IBOutlet weak var etxt_msg: UITextView!
    @IBOutlet weak var etxt_send: UIButton!
    
    @IBOutlet weak var webview: UIWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        etxt_name.delegate = self
        etxt_email.delegate = self
        etxt_msg.delegate = self
        webview.delegate = self
        webview.scrollView.isScrollEnabled = false
        self.hideKeyboardWhenTappedAround()
        let h = webview.frame.height
        let body = """
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1682.018089748153!2d-116.96787538294221!3d32.52518066423059!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xecc689978df4128!2zU8OtbnRlc2lz!5e0!3m2!1ses!2smx!4v1525843772990"  frameborder="0" style="border:0;overflow:hidden;overflow-x:hidden;overflow-y:hidden;height:\(h)px;width:100%;position:absolute;top:0px;left:0px;right:0px;bottom:0px" height="\(h)px" width="100%" scrolling="no" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen" allowfullscreen></iframe>
        """
        self.webview.loadHTMLString(htmlTemplate+body+htmlTemplate2, baseURL: nil)
        
        
        
        onClick(view: txt_page, s: #selector(self.pageClick))
        onClick(view: img_page, s: #selector(self.pageClick))
        onClick(view: txt_twitter, s: #selector(self.twitterClick))
        onClick(view: img_twitter, s: #selector(self.twitterClick))
        onClick(view: txt_facebook, s: #selector(self.fbookClick))
        onClick(view: img_facebook, s: #selector(self.fbookClick))
        onClick(view: txt_youtube, s: #selector(self.youtubeClick))
        onClick(view: img_youtube, s: #selector(self.youtubeClick))
        onClick(view: etxt_send , s: #selector(self.sendClick))
        
    }
    func onClick(view:UIView, s:Selector) {
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: s))
    }
    @objc func pageClick()  {
        UIApplication.shared
            .open(URL(string: "http://sintesistv.com.mx")!, options: [:], completionHandler: nil)
    }
    @objc func twitterClick()  {
        UIApplication.shared
            .open(URL(string: "https://twitter.com/sintesis_tv")!, options: [:], completionHandler: nil)
    }
    @objc func fbookClick()  {
        UIApplication.shared
            .open(URL(string: "https://www.facebook.com/SintesisTV")!, options: [:], completionHandler: nil)
    }
    @objc func youtubeClick()  {
        UIApplication.shared
            .open(URL(string: "https://www.youtube.com/user/SintesisTVBC")!, options: [:], completionHandler: nil)
    }
    @objc func wappClick()  {
        print("wappClick page")
    }
    
    @objc func sendClick()  {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        
        composeVC.setToRecipients(["app@sintesistv.com.mx"])
        composeVC.setSubject(etxt_email.text ?? "")
        composeVC.setMessageBody(etxt_msg.text ?? "" , isHTML: false)
        if MFMailComposeViewController.canSendMail() {
            self.present(composeVC, animated: true, completion: nil)
        }else{
            showErrors()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollview.setContentOffset(CGPoint(x:0,y:250), animated: true)
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        scrollview.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        scrollview.setContentOffset(CGPoint(x:0,y:250), animated: true)
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        scrollview.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
        return true
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print("\(navigationType.rawValue) \(request.mainDocumentURL! ) \(request.url!)" )
        
        
        if navigationType == UIWebViewNavigationType.linkClicked ||
            (!request.mainDocumentURL!.absoluteString.contains("about:blank") &&
                !request.url!.absoluteString.contains("about:blank") &&
                !request.mainDocumentURL!.absoluteString.contains("applewebdata") &&
                !request.url!.absoluteString.contains("applewebdata"))
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(request.url!, options: [:])
            } else {
                UIApplication.shared.openURL(request.url!)
            }
            return false
        }
        return true
        
    }
    func showErrors(){
        let sendErrors = UIAlertController(title: "No se puede enviar mensaje", message: "Tambien puede enviarnos un correo a app@sintesistv.com.mx", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Ok", style: .default, handler: nil)
        sendErrors.addAction(dismiss)
        self.present(sendErrors,animated: true,completion: nil)
    }
    
    private func mailComposeController(controller: MFMailComposeViewController,
                               didFinishWithResult result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    
    let htmlTemplate = """
    <!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
    </style>
  </head>
  <body style="margin:0px;padding:0px;overflow:hidden">
"""
    let htmlTemplate2="""
</body>
</html>
"""

}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer =     UITapGestureRecognizer(target: self, action:    #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


//
//  LiveVideViewController.swift
//  SintesisIOS
//
//  Created by cesar on 3/17/18.
//  Copyright © 2018 sintesis. All rights reserved.
//

import UIKit
import WebKit
class LiveVideViewController: UIViewController , UIWebViewDelegate{
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var spin_loading: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //let s = UIScreen.main.scale
       // let w = webview.frame.width
        //let h =  w*620/920
        self.webview.delegate = self
        webview.allowsInlineMediaPlayback = true 
        let body = """
        
        <iframe id=frame src="https://iframe.dacast.com/b/63852/c/88933" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height:100%;width:100%;position:absolute;top:0px;left:0px;right:0px;bottom:0px" height="100%" width="100%" scrolling="no" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>
        
        
        """
        self.webview.loadHTMLString(htmlTemplate+body+htmlTemplate2, baseURL: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        spin_loading.stopAnimating()
    }
    
    
    let htmlTemplate = """
    <!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        html{background-color:#323232;}
    </style>
  </head>
  <body style="margin:0px;padding:0px;overflow:hidden">
"""
    let htmlTemplate2="""
</body>
<script>
    function doOnOrientationChange() {
       
        switch(window.orientation) {
        
          case -90:
          case 90:
            break;
          default:
            window.location = "wsfn://nofull";
            break;
        }
    }
    window.addEventListener('orientationchange', doOnOrientationChange);
    doOnOrientationChange();
</script>
</html>
"""
}

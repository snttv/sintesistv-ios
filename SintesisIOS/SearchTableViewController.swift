//
//  SearchTableViewController.swift
//  SintesisIOS
//
//  Created by cesar on 3/26/18.
//  Copyright © 2018 sintesis. All rights reserved.
//

import UIKit

class SearchTableViewController: UITableViewController,PostTableViewCellDelegate,UISearchBarDelegate  {
    private struct PostWithImage {
        let post:PostEmbed
        let image:UIImage?
    }
    
    var posts = [PostEmbed]()
    var loadingOld:Bool=false
    var isSearching = false
    @IBOutlet weak var searchbar: UISearchBar!
    var searchText = ""
    var continueLoading=true
    let postServ = PostService()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 120
        searchbar.showsSearchResultsButton = true
        searchbar.delegate = self
        searchbar.returnKeyType = UIReturnKeyType.search
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implemee ime impe impe impe impe impe imp rç√
        if(section==0){
            return posts.count
        }else if(section==1 && loadingOld){
            return 1
        }
        return 0
        
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        
        if offsetY > contentHeight - scrollView.frame.height * 2 {
            if !loadingOld {
                addOldPosts(category:nil, num:10)
            }
        }
    }
    
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section==1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadingCell", for: indexPath) as! LoadingCell
            cell.loading(true)
            return cell
        }
        
        
        let cellIdentifier = "PostTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PostTableViewCell else {
            fatalError("The dequeued cell is not an instance of PostTableViewCell.")
        }
        let notice = posts[ indexPath.row]
        // Configure the cell...
        let photo = UIImage(named: "logo")
        cell.image_header.image = photo
        let task = notice.preloadImg(ok: {
            // cell.image_header.sd_setImage(with: URL(string: indexPath.section == 0  ? notice.headerImgInfo!.mediumUrl! : notice.headerImgInfo!.largeUrl!), placeholderImage: UIImage(named: "logo"))
            let original = (indexPath.section == 0  ? notice.headerImgInfo!.mediumUrl : notice.headerImgInfo!.largeUrl) ?? notice.headerImgInfo?.fullUrl ?? ""
            if original==""{ return}
            if let encoded = original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let url = URL(string: encoded)
            {
                cell.image_header.sd_setImage(with:url, completed: {
                    image,error,cache,url in
                    if error != nil {
                        cell.image_header.sd_setImage(with: URL(string:notice.headerImgInfo!.fullUrl!), placeholderImage: UIImage(named: "logo"))
                    }
                })
            }
            
        }, errorFn: nil)
        if task != nil{
            cell.tasks.append(task!)
        }
        cell.txt_date?.text = notice.dateFormat(format: "dd/MM/yyyy")
        
        let task2 = notice.preloadCat(ok:{
            DispatchQueue.main.async {
                cell.txt_cat.text = notice.category?.name
            }
        } , errorFn:nil)
        if task2 != nil{
            cell.tasks.append(task2!)
        }
        
        cell.txt_title.text = notice.title.rendered
            .replacingOccurrences(of: "&#8216;", with: "’")
            .replacingOccurrences(of: "&#8217;", with: "’")
            .replacingOccurrences(of: "&#8220;", with: "“")
            .replacingOccurrences(of: "&#8221;", with: "”")
            .replacingOccurrences(of: "&#8230;", with: "…")
            .replacingOccurrences(of: "&#038;", with: "&")
        if(cell.txt_notice_short != nil){
            do {
                let attrStr = try NSAttributedString(
                    data: notice.excerpt.rendered.data(using: .unicode, allowLossyConversion: true)!,
                    options:[.documentType: NSAttributedString.DocumentType.html,
                             .characterEncoding: String.Encoding.utf8.rawValue],
                    documentAttributes: nil)
                cell.txt_notice_short?.attributedText = attrStr
            } catch _ {
                cell.txt_notice_short?.text = notice.excerpt.rendered
            }
        }
        cell.delegate = self
        
        return cell
    }
    
    func onOpenPost(_ sender: PostTableViewCell) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else { return }
        print("Open", sender, tappedIndexPath)
        let post = posts[tappedIndexPath.row]
        performSegue(withIdentifier: "GoToFullSearchPostSegue", sender:PostWithImage(post: post,image:sender.image_header?.image ))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is FullPostTableViewController && sender is PostWithImage
        {
            let vc = segue.destination as? FullPostTableViewController
            let post = sender as! PostWithImage
            vc?.postEmbed = post.post
            vc?.preImg = post.image
        }
    }
    
    func getPosts(category:CategoryModel? = nil, addToPost:Bool=false){
        
        if(searchText.isEmpty) {return}
        var query = PostQuery();
        query.category = category
        query.search = searchText
        
        postServ.postWithQuery(query: query, ok: {
            (jsonList:[PostEmbed]) in
            if !jsonList.isEmpty {
                DispatchQueue.main.async {
                    self.posts = jsonList
                    self.tableView.reloadData()
                }
            }else{
                self.continueLoading = false
            }
            
        }, errorFn:{ _ in self.errorAlert(msg: "Error al cargar noticias")})
    }
    
    func addOldPosts(category:CategoryModel? = nil,num:Int=1){
        if(!loadingOld && continueLoading){
            loadingOld=true
            tableView.reloadSections(IndexSet(integer: 1), with: .none)
            var query = PostQuery();
            query.category = category
            query.len = num
            query.beforeDate = posts.last?.date
            query.search = posts.last?.date
            
            postServ.postWithQuery(query: query, ok: {
                (jsonList:[PostEmbed]) in
                if(!jsonList.isEmpty){
                    jsonList.forEach{
                        e in
                        e.preloadImg()
                        e.preloadCat()
                    }
                    let indexPaths = (0..<jsonList.count).map { IndexPath(row: self.posts.count-1+$0, section: 0) }
                    self.posts += jsonList
                    DispatchQueue.main.async {
                        self.tableView.beginUpdates()
                        
                        self.tableView.insertRows(at: indexPaths, with: .none)
                        self.tableView.endUpdates()
                        self.loadingOld=false
                    }
                }else{
                    self.continueLoading=false
                }
                
            },errorFn:{ _ in
                self.continueLoading=false
                self.loadingOld=false
                self.errorAlert(msg: "Error al cargar noticias")
            })
        }
    }
    
    private func errorAlert(msg:String, force:Bool = false){
        DispatchQueue.main.async {
            if !force && !self.continueLoading  {return}
            let alert = UIAlertController(title: "No se puede completar acción", message:
                msg, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if var text = searchbar.text {
            text = text.trimmingCharacters(in: .whitespacesAndNewlines)
            if(!text.isEmpty){
                searchText = text
                continueLoading=true
                getPosts()
            }
        }
        searchbar.endEditing(true)
    }


}

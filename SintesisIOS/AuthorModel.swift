//
//  AuthorModel.swift
//  SintesisIOS
//
//  Created by cesar on 3/14/18.
//

import Foundation

class AuthorModel: Codable {
    let id: Int
    let name: String
    let url: String
    let description: String
    let link: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case url = "url"
        case description = "description"
        case link = "link"
    }
    
    init(id: Int, name: String, url: String, description: String, link: String
        ) {
        self.id = id
        self.name = name
        self.url = url
        self.description = description
        self.link = link
      }
}




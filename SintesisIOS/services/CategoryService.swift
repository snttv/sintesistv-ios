//
//  CategoryService.swift
//  SintesisIOS
//
//  Created by cesar on 5/24/18.
//  Copyright © 2018 sintesis. All rights reserved.
//

import Foundation

class CategoryService {
    // categorias de la ventana principal + las subcategorias de "Noticias"
    @discardableResult
    public func mainCategories(ok:(([CategoryModel]) -> Void)?,errorFn:((Error) -> Void)?) -> URLSessionDataTask{
        return getJsonList(urlStr: "http://sintesistv.com.mx/wp-json/wp/v2/categories?parent=0&exclude=1&hide_empty=true", ok: {
            (jsonList:CategoryList) in
            var mainList = [CategoryModel(id: 0, name: "Recientes", parent: 0)] + jsonList
            
            let notCatId = mainList.index(where: {
                e in e.name.lowercased() == "noticias"
            })
            if(notCatId != nil){
                let notCat = mainList.remove(at: notCatId!)
                self.noticeSubcategoriesFrom(category: notCat,ok:{l in
                    var sublist = l
                    
                    let catId = sublist.index(where: {
                        e in e.name.lowercased() == "opinión"
                    })
                    if(catId != nil){
                        sublist.remove(at: catId!)
                    }
                    let recents = mainList.remove(at: 0)
                    ok?([recents] + sublist + mainList)
                    
                },errorFn:errorFn)
            }else{
                ok?(mainList)
            }
            
        },errorFn:errorFn)
    }
    @discardableResult
    func noticeSubcategoriesFrom(category:CategoryModel ,ok:(([CategoryModel]) -> Void)?,errorFn:((Error) -> Void)?)-> URLSessionDataTask{
        return getJsonList(urlStr: "http://sintesistv.com.mx/wp-json/wp/v2/categories?parent=\(category.id)&exclude=1&hide_empty=true", ok: {
            ( jsonList:CategoryList) in
            ok?(jsonList)
        },errorFn:errorFn)
    }
    
    
    
}

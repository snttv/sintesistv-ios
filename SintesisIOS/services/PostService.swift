//
//  PostService.swift
//  SintesisIOS
//
//  Created by cesar on 5/24/18.
//  Copyright © 2018 sintesis. All rights reserved.
//

import Foundation
class PostService {
    @discardableResult
    public func posts(category:CategoryModel?,ok:(([PostEmbed]) -> Void)?,errorFn:((Error) -> Void)?) -> URLSessionDataTask{
        var url:String = "http://sintesistv.com.mx/wp-json/wp/v2/posts?context=embed"
        if let category = category {
            if category.id != 0{
                url += "&categories=\(category.id)"
            }
        }
        return getJsonList(urlStr: url, ok: {
            (jsonList:[PostEmbed]) in
            ok?(jsonList)
        },errorFn:errorFn)
    }
    @discardableResult
    public func postWithQuery(query:PostQuery,ok:(([PostEmbed]) -> Void)?,errorFn:((Error) -> Void)?) -> URLSessionDataTask{
        var len = query.len ?? 10
        if len>30 {len=30}
        else if len<1 {len=1}
        let url = "http://sintesistv.com.mx/wp-json/wp/v2/posts?"
            + "per_page=\(len)"
            + (query.fullPost ? "&context=embed" : "")
            + (query.beforeDate != nil ? "&before=\(query.beforeDate!)" : "")
            + (query.category != nil && query.category!.id > 0 ? "&categories=\(query.category!.id)" : "" )
            + (query.search != nil && !(query.search!.isEmpty) ? "&search=\(query.search!)" : "" )
            + (query.id != nil ? "&include=\(query.id!)": "" )
        return getJsonList(urlStr: url, ok: {
            (jsonList:[PostEmbed]) in
            ok?(jsonList)
        },errorFn:errorFn)
    }
    @discardableResult
    public func fullPostWithQuery(query:PostQuery,ok:(([Post]) -> Void)?,errorFn:((Error) -> Void)?) -> URLSessionDataTask{
        var len = query.len ?? 10
        if len>30 {len=30}
        else if len<1 {len=1}
        let url = "http://sintesistv.com.mx/wp-json/wp/v2/posts?"
            + "per_page=\(len)"
            + (query.fullPost ? "&context=embed" : "")
            + (query.beforeDate != nil ? "&before=\(query.beforeDate!)" : "")
            + (query.category != nil && query.category!.id > 0 ? "&categories=\(query.category!.id)" : "" )
            + (query.search != nil && !(query.search!.isEmpty) ? "&search=\(query.search!)" : "" )
            + (query.id != nil ? "&include=\(query.id!)": "" )
        return getJsonList(urlStr: url, ok: {
            (jsonList:[Post]) in
            ok?(jsonList)
        },errorFn:errorFn)
    }
    
    
}

struct PostQuery {
    var category:CategoryModel? = nil
    var len:Int? = nil
    var beforeDate:String? = nil//podria ser tipo fecha
    var fullPost:Bool=false
    var search:String? = nil
    var id:Int? = nil
    
}

//
//  NetUtil.swift
//  SintesisIOS
//
//  Created by cesar on 3/14/18.
//  Copyright © 2018 sintesis. All rights reserved.
//


import Foundation
@discardableResult
public func getJsonList<JsonT>(urlStr:String,ok:(([JsonT]) -> Void)?,errorFn:((Error) -> Void)?) -> URLSessionDataTask
    where JsonT : Codable {
    return get(urlStr: urlStr, ok: {
        jsonData in
        do {
            let list = try JSONDecoder().decode([JsonT].self, from: jsonData)
            ok?(list)
        } catch {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Error JSON decode"]) as Error
            errorFn?(error)
        }
    },errorFn: errorFn)
}
@discardableResult
public func getJsonObject<JsonT>(urlStr:String,ok:((JsonT) -> Void)?,errorFn:((Error) -> Void)?) -> URLSessionDataTask
    where JsonT : Codable {
    return get(urlStr: urlStr, ok: {
        jsonData in
        do {
            let obj = try JSONDecoder().decode(JsonT.self, from: jsonData)
            ok?(obj)
        } catch {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Error JSON decode"]) as Error
            errorFn?(error)
        }
    },errorFn: errorFn)
}

@discardableResult
public func get(urlStr:String,ok:((Data) -> Void)?,errorFn:((Error) -> Void)?) -> URLSessionDataTask{
    let s = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
    guard  let url = URL(string: s)  else { fatalError("Could not create URL ") }
    var request = URLRequest(url: url)
    request.httpMethod = "GET"
    let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
        if let error = error {
            errorFn?(error)
        } else if let data = data {
            ok?(data)
        }else {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Data was not retrieved from request"]) as Error
            //fatalError("error: \(error.localizedDescription)")
            errorFn?(error)
        }
    }
    task.resume()
    return task
}







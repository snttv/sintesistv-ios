//
//  LoadingCell.swift
//  SintesisIOS
//
//  Created by cesar on 4/30/18.
//  Copyright © 2018 sintesis. All rights reserved.
//

import UIKit

class LoadingCell: UITableViewCell {

    @IBOutlet weak var spinner: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func loading(_ isLoading:Bool)  {
        if isLoading {
             spinner.startAnimating()
        }else{
            spinner.stopAnimating()
        }
       
    }

}

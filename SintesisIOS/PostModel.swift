// To parse the JSON, add this file to your project and do:
//
//   let post = try Post(json)
import UIKit
import Foundation
class HeaderImageInfo{
    let minUrl:String?
    let mediumUrl:String?
    let largeUrl:String?
    let fullUrl:String?
    init(min:String?,medium:String?,large:String?,full:String?) {
        minUrl=min;
        mediumUrl=medium;
        largeUrl=large;
        fullUrl=full;
    }
}

typealias PostEmbedtList = [PostEmbed]
class PostEmbed: Codable {
    let id: Int
    let date: String
    let title: Content
    let excerpt: Content
    let author: Int
    let featuredMedia: Int
    let links: Links
    var headerImgInfo:HeaderImageInfo?=nil
    var category:CategoryModel?=nil
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case date = "date"
        case title = "title"
        case excerpt = "excerpt"
        case author = "author"
        case featuredMedia = "featured_media"
        case links = "_links"
    }
    
    init(id: Int, date: String,
         title: Content, excerpt: Content, author: Int, featuredMedia: Int, links: Links) {
        self.id = id
        self.date = date
        self.title = title
        self.excerpt = excerpt
        self.author = author
        self.featuredMedia = featuredMedia
        self.links = links
    }
    
    func dateFormat(format:String = "dd/MM/yyyy HH:mm") -> String {
        let dateFor = DateFormatter()
        dateFor.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        if let date = dateFor.date(from: self.date) {
            dateFor.dateFormat = format
            let txt = dateFor.string(from: date)
            return txt
        }else{
            return self.date
        }
    }
    
    @discardableResult
    func preloadImg(ok:(() -> Void)? = nil,errorFn:((Error) -> Void)? = nil) ->URLSessionDataTask?{
        var task: URLSessionDataTask? = nil
        if(headerImgInfo != nil){
            ok?()
            return task
        }
        task = getJsonObject(urlStr: "http://sintesistv.com.mx/wp-json/wp/v2/media/\(self.featuredMedia)", ok: { (photoJson:PhotoModel) in
            self.headerImgInfo = HeaderImageInfo(min:photoJson.mediaDetails?.sizes["thumbnail"]?.sourceURL ,
                                                 medium:photoJson.mediaDetails?.sizes["medium"]?.sourceURL ,
                                                 large:photoJson.mediaDetails?.sizes["large"]?.sourceURL,
                                                 full:photoJson.mediaDetails?.sizes["full"]?.sourceURL
            )
            ok?()
            
        },errorFn:errorFn)
        return task
    }
    @discardableResult
    func preloadCat(ok:(() -> Void)? = nil,errorFn:((Error) -> Void)? = nil) ->URLSessionDataTask?{
        var task: URLSessionDataTask? = nil
        if(category != nil){
            ok?()
            return task
        }
        task = getJsonObject(urlStr: "http://sintesistv.com.mx/wp-json/wp/v2/categories?post=\(self.id)", ok:{
            (c:[CategoryModel]) in
                self.category = c.first
        },errorFn:errorFn)
        return task
    }
}

typealias PostList = [Post]
class Post: Codable {
    let id: Int
    let date: String
    let dateGmt: String
    let title: Content
    let content: Content
    let excerpt: Content
    let author: Int
    let featuredMedia: Int
    let sticky: Bool
    let categories: [Int]
    let tags: [Int]
    let links: Links
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case date = "date"
        case dateGmt = "date_gmt"
        case title = "title"
        case content = "content"
        case excerpt = "excerpt"
        case author = "author"
        case featuredMedia = "featured_media"
        case sticky = "sticky"
        case categories = "categories"
        case tags = "tags"
        case links = "_links"
    }
    
    init(id: Int, date: String, dateGmt: String,
         title: Content, content: Content, excerpt: Content, author: Int, featuredMedia: Int,
         sticky: Bool,
         categories: [Int], tags: [Int], links: Links) {
        self.id = id
        self.date = date
        self.dateGmt = dateGmt
        self.title = title
        self.content = content
        self.excerpt = excerpt
        self.author = author
        self.featuredMedia = featuredMedia
        self.sticky = sticky
        self.categories = categories
        self.tags = tags
        self.links = links
    }
}

class Content: Codable {
    let rendered: String
    
    enum CodingKeys: String, CodingKey {
        case rendered = "rendered"
    }
    
    init(rendered: String) {
        self.rendered = rendered
    }
}

class Links: Codable {
    let linksSelf: [Link]
    let author: [Link]
    
    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case author = "author"
    }
    
    init(linksSelf: [Link],
        author: [Link]
        ) {
        self.linksSelf = linksSelf
        self.author = author
    }
}


class Link: Codable {
    let href: String
    
    enum CodingKeys: String, CodingKey {
        case href = "href"
    }
    
    init(href: String) {
        self.href = href
    }
}

class Cury: Codable {
    let name: String
    let href: String
    let templated: Bool
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case href = "href"
        case templated = "templated"
    }
    
    init(name: String, href: String, templated: Bool) {
        self.name = name
        self.href = href
        self.templated = templated
    }
}

class WpTerm: Codable {
    let taxonomy: String
    let embeddable: Bool
    let href: String
    
    enum CodingKeys: String, CodingKey {
        case taxonomy = "taxonomy"
        case embeddable = "embeddable"
        case href = "href"
    }
    
    init(taxonomy: String, embeddable: Bool, href: String) {
        self.taxonomy = taxonomy
        self.embeddable = embeddable
        self.href = href
    }
}

// MARK: Encode/decode helpers

class JSONNull: Codable {
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String
    
    required init?(intValue: Int) {
        return nil
    }
    
    required init?(stringValue: String) {
        key = stringValue
    }
    
    var intValue: Int? {
        return nil
    }
    
    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {
    public let value: Any
    
    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }
    
    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }
    
    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }
    
    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }
    
    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }
    
    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }
    
    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }
    
    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }
    
    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }
    
    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }
    
    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}

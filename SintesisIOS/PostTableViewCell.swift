//
//  NoticeCell.swift
//  SintesisIOS
//
//  Created by cesar on 2/27/18.
//  Copyright © 2018 sintesis. All rights reserved.
//

import Foundation
import UIKit
class PostTableViewCell: UITableViewCell {
    @IBOutlet weak var image_header: UIImageView!
    @IBOutlet weak var txt_title: UILabel!
    @IBOutlet weak var txt_notice_short: UILabel?
    @IBOutlet weak var cellview: UIView!
    @IBOutlet weak var txt_date: UILabel?
    @IBOutlet weak var txt_cat: UILabel!
    
    
    
    var tasks:[URLSessionDataTask] = []
    weak var delegate :PostTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(openPost(_:)))
        addGestureRecognizer(tapGesture)
        image_header.image = nil
        let s = UIScreen.main.scale
        let h =  UIScreen.main.bounds.height
        print("\(h) \(s) \(h*s)")
        if h < 650.0{
            txt_title.numberOfLines = 3
        }
        
        //667.0 2.0 1334.0 i6
        //568.0 2.0 1136.0 i5s
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func openPost(_ sender: UITapGestureRecognizer){
        delegate?.onOpenPost(self)
    }
    
    override func prepareForReuse() {
        tasks.forEach{ t in t.cancel()}
        image_header.sd_cancelCurrentImageLoad()
        image_header.image = nil
    }
    
}
protocol PostTableViewCellDelegate : class {
    func onOpenPost(_ sender:PostTableViewCell)
}

//
//  ImageModel.swift
//  SintesisIOS
//
//  Created by cesar on 3/12/18.
//

import Foundation
import Foundation

class PhotoModel: Codable {
    let id: Int
    let date, dateGmt: String
    let guid: Content
    let modified, modifiedGmt, slug, status: String?
    let type, link: String?
    let title: Content
    let author: Int
    let commentStatus, pingStatus, template: String?
    let meta: [JSONAny]?
    let description, caption: Content?
    let altText, mediaType, mimeType: String?
    let mediaDetails: MediaDetails?
    let post: Int?
    let sourceURL: String?
    let links: LinksPhoto
    
    enum CodingKeys: String, CodingKey {
        case id, date
        case dateGmt = "date_gmt"
        case guid, modified
        case modifiedGmt = "modified_gmt"
        case slug, status, type, link, title, author
        case commentStatus = "comment_status"
        case pingStatus = "ping_status"
        case template, meta, description, caption
        case altText = "alt_text"
        case mediaType = "media_type"
        case mimeType = "mime_type"
        case mediaDetails = "media_details"
        case post
        case sourceURL = "source_url"
        case links = "_links"
    }
    
    init(id: Int, date: String, dateGmt: String, guid: Content, modified: String?, modifiedGmt: String?, slug: String?, status: String?, type: String?, link: String?, title: Content, author: Int, commentStatus: String?, pingStatus: String?, template: String?, meta: [JSONAny]?, description: Content?, caption: Content?, altText: String?, mediaType: String?, mimeType: String?, mediaDetails: MediaDetails?, post: Int?, sourceURL: String?, links: LinksPhoto) {
        self.id = id
        self.date = date
        self.dateGmt = dateGmt
        self.guid = guid
        self.modified = modified
        self.modifiedGmt = modifiedGmt
        self.slug = slug
        self.status = status
        self.type = type
        self.link = link
        self.title = title
        self.author = author
        self.commentStatus = commentStatus
        self.pingStatus = pingStatus
        self.template = template
        self.meta = meta
        self.description = description
        self.caption = caption
        self.altText = altText
        self.mediaType = mediaType
        self.mimeType = mimeType
        self.mediaDetails = mediaDetails
        self.post = post
        self.sourceURL = sourceURL
        self.links = links
    }
}



class LinksPhoto: Codable {
    let linksSelf : [Link]
     let collection, about: [Link]?
    let author, replies: [Link]?
    
    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case collection, about, author, replies
    }
    
    init(linksSelf: [Link], collection: [Link]?, about: [Link]?, author: [Link]?, replies: [Link]?) {
        self.linksSelf = linksSelf
        self.collection = collection
        self.about = about
        self.author = author
        self.replies = replies
    }
}

class MediaDetails: Codable {
    let width, height: Int
    let file: String
    let sizes: [String: Size]
    let imageMeta: ImageMeta
    
    enum CodingKeys: String, CodingKey {
        case width, height, file, sizes
        case imageMeta = "image_meta"
    }
    
    init(width: Int, height: Int, file: String, sizes: [String: Size], imageMeta: ImageMeta) {
        self.width = width
        self.height = height
        self.file = file
        self.sizes = sizes
        self.imageMeta = imageMeta
    }
}

class ImageMeta: Codable {
    let aperture, credit, camera, caption: String
    let createdTimestamp, copyright, focalLength, iso: String
    let shutterSpeed, title, orientation: String
    let keywords: [JSONAny]
    
    enum CodingKeys: String, CodingKey {
        case aperture, credit, camera, caption
        case createdTimestamp = "created_timestamp"
        case copyright
        case focalLength = "focal_length"
        case iso
        case shutterSpeed = "shutter_speed"
        case title, orientation, keywords
    }
    
    init(aperture: String, credit: String, camera: String, caption: String, createdTimestamp: String, copyright: String, focalLength: String, iso: String, shutterSpeed: String, title: String, orientation: String, keywords: [JSONAny]) {
        self.aperture = aperture
        self.credit = credit
        self.camera = camera
        self.caption = caption
        self.createdTimestamp = createdTimestamp
        self.copyright = copyright
        self.focalLength = focalLength
        self.iso = iso
        self.shutterSpeed = shutterSpeed
        self.title = title
        self.orientation = orientation
        self.keywords = keywords
    }
}

class Size: Codable {
    let file: String
    let width, height: Int
    let mimeType, sourceURL: String
    
    enum CodingKeys: String, CodingKey {
        case file, width, height
        case mimeType = "mime_type"
        case sourceURL = "source_url"
    }
    
    init(file: String, width: Int, height: Int, mimeType: String, sourceURL: String) {
        self.file = file
        self.width = width
        self.height = height
        self.mimeType = mimeType
        self.sourceURL = sourceURL
    }
}

// MARK: Convenience initializers

extension PhotoModel {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(PhotoModel.self, from: data)
        self.init(id: me.id, date: me.date, dateGmt: me.dateGmt, guid: me.guid, modified: me.modified, modifiedGmt: me.modifiedGmt, slug: me.slug, status: me.status, type: me.type, link: me.link, title: me.title, author: me.author, commentStatus: me.commentStatus, pingStatus: me.pingStatus, template: me.template, meta: me.meta, description: me.description, caption: me.caption, altText: me.altText, mediaType: me.mediaType, mimeType: me.mimeType, mediaDetails: me.mediaDetails, post: me.post, sourceURL: me.sourceURL, links: me.links)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}



extension LinksPhoto {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(LinksPhoto.self, from: data)
        self.init(linksSelf: me.linksSelf, collection: me.collection, about: me.about, author: me.author, replies: me.replies)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension MediaDetails {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(MediaDetails.self, from: data)
        self.init(width: me.width, height: me.height, file: me.file, sizes: me.sizes, imageMeta: me.imageMeta)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension ImageMeta {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(ImageMeta.self, from: data)
        self.init(aperture: me.aperture, credit: me.credit, camera: me.camera, caption: me.caption, createdTimestamp: me.createdTimestamp, copyright: me.copyright, focalLength: me.focalLength, iso: me.iso, shutterSpeed: me.shutterSpeed, title: me.title, orientation: me.orientation, keywords: me.keywords)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Size {
    convenience init(data: Data) throws {
        let me = try JSONDecoder().decode(Size.self, from: data)
        self.init(file: me.file, width: me.width, height: me.height, mimeType: me.mimeType, sourceURL: me.sourceURL)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try JSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}


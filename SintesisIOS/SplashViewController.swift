//
//  SplashViewController.swift
//  SintesisIOS
//
//  Created by cesar on 5/23/18.
//  Copyright © 2018 sintesis. All rights reserved.
//

import UIKit
import Reachability
class SplashViewController: UIViewController {
    override var prefersStatusBarHidden: Bool {
        return false
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    var posts = [PostEmbed]()
    var destination: UIViewController?
    let reachability = Reachability()!
    var continueLoading=true
    var postServ = PostService()
    @IBOutlet weak var progbar_loading: UIProgressView!
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        ini()
        // Do any additional setup after loading the view.
    }
    
    func ini(){
        continueLoading=true
        print(" connected??")
        if(reachability.connection != .none){
            print("is connected")
            
            reachability.whenUnreachable = { _ in
                self.errorAlertAndReload()
                    
            }
            do {
                try reachability.startNotifier()
            } catch {
                print("Unable to start\nnotifier")
            }
                
            getPosts()
        }else{
            self.errorAlertAndReload()
        }
        progbar_loading.transform = progbar_loading.transform.scaledBy(x: 1, y: 2)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is PostTableViewController && sender is [PostEmbed]
        {
            let vc = segue.destination as? PostTableViewController
            vc?.posts = sender as! [PostEmbed]

        }
    }
    func getPosts(category:CategoryModel? = nil, addToPost:Bool=false){
        postServ.posts(category: nil, ok: {
            jsonList in
            if !jsonList.isEmpty {
                var i = 0;
                self.posts = jsonList
                self.posts.forEach({ p in
                    p.preloadCat()
                    p.preloadImg(ok: {
                        DispatchQueue.main.async {
                            i+=1
                            print(i)
                            self.progbar_loading.progress = Float(i)/Float(jsonList.count)
                            if(i>=10){
                                self.performSegue(withIdentifier: "GoToPostsSegue", sender:self.posts )
                            }
                        }
                    }, errorFn: { _ in
                        DispatchQueue.main.async {
                            i+=1
                            print("error(\(i))")
                            if(i>=10){
                                self.performSegue(withIdentifier: "GoToPostsSegue", sender:self.posts )
                            }
                        }
                    })
                })
            }else{
                self.errorAlert(msg: "Error al cargar noticias")
            }
        },errorFn:{ _ in self.errorAlert(msg: "Error al cargar noticias")})
        
    }
    private func errorAlertAndReload(){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error en conexión", message:
                "verifique que tenga conexión a internet", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: {_ in self.ini()}))
            
            self.present(alert, animated: true, completion: nil)
            self.continueLoading = false
            print("No Reachable ")
        }
    }
    private func errorAlert(msg:String){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error al cargar noticias", message:
                "intentelo mas tarde", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Reintentar", style: UIAlertActionStyle.default,handler: {_ in self.ini()}))
            self.present(alert, animated: true, completion: nil)
        }
    }


}

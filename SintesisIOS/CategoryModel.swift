//
//  CategoryModel.swift
//  SintesisIOS
//
//  Created by cesar on 2/27/18.
//  Copyright © 2018 sintesis. All rights reserved.
//

import Foundation

typealias CategoryList = [CategoryModel]

class CategoryModel: Codable {
    let id: Int
    let name: String
    let parent: Int
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case parent = "parent"
    }
    
    init(id: Int, name: String, parent: Int) {
        self.id = id
        self.name = name
        self.parent = parent
    }
}


                            //
//  NoticeTableViewController.swift
//  SintesisIOS
//
//  Created by cesar on 2/27/18.
//  Copyright © 2018 sintesis. All rights reserved.
//

import UIKit
import Dropdowns
import  SDWebImage
import Reachability
                            
enum PostViewModelItemType {
    case compactPost
    case bigPost
    case header
}
                            
protocol PostViewModelItem {
    
    var rowCount: Int { get }
    var posts:[PostEmbed]{get set}
}
extension PostViewModelItem {
    var type: PostViewModelItemType { return .compactPost }
    var rowCount: Int {
        return 1
    }
}

class PostViewModelCompactPostItem: PostViewModelItem{
    var posts:[PostEmbed]
    init(posts:[PostEmbed]) {
        self.posts = posts
    }
    var rowCount: Int{return posts.count}

}
class PostViewModelBigPostItem: PostViewModelItem{
    var posts:[PostEmbed]
    init(posts:[PostEmbed]) {
        self.posts = posts
    }
    var type: PostViewModelItemType{
        return .bigPost;
    }
    var rowCount: Int{return posts.count}

}
class PostViewModelHeaderItem: PostViewModelItem{
    var posts:[PostEmbed]
    init(posts:[PostEmbed]) {
        self.posts = posts
    }
    var type: PostViewModelItemType{
        return .header;
    }
    var rowCount: Int{return posts.count}
}
                            


class PostTableViewController: UITableViewController,PostTableViewCellDelegate{
    private struct PostWithImage {
        let post:PostEmbed
        let image:UIImage?
    }
    
    var headerPost:PostEmbed?=nil
    var posts = [PostEmbed]()
    var categories = CategoryList()
    var category:CategoryModel? = nil
    var loadingOld:Bool=false
    var cacheImage:UIImageView = UIImageView()
    let reachability = Reachability()!
    let catServ = CategoryService()
    let postServ = PostService()
    var conectionOk=true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        }
        reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                if self.loadingOld {
                    self.errorAlert(msg: "Fallo en conexiøn de internet",force: true)
                }
                
                self.conectionOk = false
                print("No Reachable ")
                
            }
        }
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start\nnotifier")
        }
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 120
        if(posts.isEmpty){
            getPosts()
        }else{
            self.headerPost = self.posts.removeFirst()
            self.tableView.reloadData()
        }
        
        catServ.mainCategories( ok: {
            cats in
            self.categories = cats
            self.initCategoryMenu()
        }, errorFn: {_ in self.errorAlert(msg: "Error al cargar categorias")})
        
        Config.ArrowButton.Text.color = UIColor.white
        Config.List.backgroundColor = UIColor(rgb: 0x005dcc).withAlphaComponent(0.8)
        let items:[String] = ["Noticias"]
        let titleView = TitleView(navigationController: navigationController!, title: "Noticias", items: items)
        navigationItem.titleView = titleView
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implemee ime impe impe impe impe impe imp rç√
        if(section==0){
            return headerPost==nil ? 0 : 1
        }else if(section==1){
            return posts.count
        }else if(section==2 && loadingOld){
            return 1
        }
        return 0
        
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height - scrollView.frame.height + 20
        
        
        if offsetY > contentHeight {
            if !loadingOld {
                addOldPosts(category:self.category, num:10)
            }
        }
    }
            
    

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section==2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadingCell", for: indexPath) as! LoadingCell
            cell.loading(true)
            return cell
        }
        
        
        let cellIdentifier = (indexPath.section==0 ) ? "HeaderPostTableViewCell" : "PostTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PostTableViewCell else {
            fatalError("The dequeued cell is not an instance of PostTableViewCell.")
        }
        let notice =  (indexPath.section==0) ? headerPost! :  posts[ indexPath.row]
        // Configure the cell...
        let photo = UIImage(named: "logo")
        cell.image_header.image = photo
        let task = notice.preloadImg(ok: {
            let original = (indexPath.section == 0  ? notice.headerImgInfo!.mediumUrl : notice.headerImgInfo!.largeUrl) ?? notice.headerImgInfo?.fullUrl ?? ""
            if original==""{ return}
            if let encoded = original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let url = URL(string: encoded)
            {
                cell.image_header.sd_setImage(with:url, completed: {
                    image,error,cache,url in
                    if error != nil {
                        cell.image_header.sd_setImage(with: URL(string:notice.headerImgInfo!.fullUrl!), placeholderImage: UIImage(named: "logo"))
                    }
                })
            }
            
        }, errorFn: nil)
        if task != nil{
            cell.tasks.append(task!)
        }
        cell.txt_date?.text = notice.dateFormat(format: "dd/MM/yyyy")
        let task2 = notice.preloadCat(ok:{
            DispatchQueue.main.async {
                cell.txt_cat.text = notice.category?.name
            }
        } , errorFn:nil)
        if task2 != nil{
            cell.tasks.append(task2!)
        }
        
        //cell.image_header.image = photo
        cell.txt_title.text = notice.title.rendered
            .replacingOccurrences(of: "&#8216;", with: "’")
            .replacingOccurrences(of: "&#8217;", with: "’")
            .replacingOccurrences(of: "&#8220;", with: "“")
            .replacingOccurrences(of: "&#8221;", with: "”")
            .replacingOccurrences(of: "&#8230;", with: "…")
            .replacingOccurrences(of: "&#038;", with: "&")
        
        
        
        if(cell.txt_notice_short != nil){
            do {
                let attrStr = try NSAttributedString(
                    data: notice.excerpt.rendered.data(using: .unicode, allowLossyConversion: true)!,
                    options:[.documentType: NSAttributedString.DocumentType.html,
                             .characterEncoding: String.Encoding.utf8.rawValue],
                    documentAttributes: nil)
                cell.txt_notice_short?.attributedText = attrStr
            } catch _ {
                cell.txt_notice_short?.text = notice.excerpt.rendered
            }
        }
        
        

        cell.delegate = self
        
        return cell
    }
    
    func onOpenPost(_ sender: PostTableViewCell) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else { return }
        print("Open", sender, tappedIndexPath)
        let post = tappedIndexPath.section==0 ? headerPost : posts[tappedIndexPath.row]
        
        performSegue(withIdentifier: "GoToFullPostSegue", sender: PostWithImage(post: post!,image:sender.image_header?.image) )
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is FullPostTableViewController && sender is PostWithImage
        {
            let vc = segue.destination as? FullPostTableViewController
            let post = sender as! PostWithImage
            vc?.postEmbed = post.post
            vc?.preImg = post.image
        }
    }
    
    private func initCategoryMenu() {
        let items = self.categories.map{e in e.name}
        DispatchQueue.main.async {
            let titleView = TitleView(navigationController: self.navigationController!, title: "Recientes", items: items)
            titleView?.action = { [weak self] index in
                self?.category = self?.categories[index]
                self?.getPosts(category: self?.category)
            }
            self.navigationItem.titleView = titleView
        }
    }

    func getPosts(category:CategoryModel? = nil, addToPost:Bool=false){
        postServ.posts(category: category, ok: {
            list in
            if !list.isEmpty {
                var tmp = list
                DispatchQueue.main.async {
                    self.headerPost = tmp.removeFirst()
                    self.posts = tmp
                    self.tableView.reloadData()
                }
            }
        }, errorFn:{ _ in self.errorAlert(msg: "Error al cargar noticias")})
    }
    
    func addOldPosts(category:CategoryModel? = nil,num:Int=1){
        if !conectionOk {
            self.errorAlert(msg: "Fallo en conexión de internet",force: true)
            return
        }
        if(!loadingOld){
            loadingOld=true
            tableView.reloadSections(IndexSet(integer: 2), with: .fade)
            var query = PostQuery();
            query.category = category
            query.len = num
            query.beforeDate = posts.last?.date
            postServ.postWithQuery(query: query, ok: {
                (jsonList:[PostEmbed]) in
                if(!jsonList.isEmpty){
                    jsonList.forEach{
                        e in
                        e.preloadImg()
                        e.preloadCat()
                    }
                    let indexPaths = (0..<jsonList.count).map { IndexPath(row: self.posts.count-1+$0, section: 1) }
                    self.posts += jsonList
                    DispatchQueue.main.async {
                        
                        self.tableView.beginUpdates()
                        self.tableView.insertRows(at: indexPaths, with: .fade)
                        self.tableView.endUpdates()
                        self.loadingOld=false
                    }
                }
                
            },errorFn:{ _ in
                self.loadingOld=false
                self.errorAlert(msg: "Error al cargar noticias")
            })
        }
    }
    private func errorAlert(msg:String, force:Bool = false){
        DispatchQueue.main.async {
            if !force && !self.conectionOk  {return}
            let alert = UIAlertController(title: "No se puede completar acción", message:
                msg, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
   
}


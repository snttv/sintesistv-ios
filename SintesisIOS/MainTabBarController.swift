//
//  MainTabBarController.swift
//  SintesisIOS
//
//  Created by cesar on 2/27/18.
//  Copyright © 2018 sintesis. All rights reserved.
//

import Foundation
import UIKit
class MainTabBarController: UITabBarController,UITabBarControllerDelegate  {
    override var prefersStatusBarHidden: Bool {
        return false
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBOutlet weak var mainTabBar: UITabBar!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //UIApplication.shared.statusBarStyle = .lightContent
        setNeedsStatusBarAppearanceUpdate()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
        self.delegate = self
        //mainTabBar.tintColor = UIColor(rgb: 0x112d80)
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swiped))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        let _ = self.viewControllers?[1]
    }
    
    @objc func swiped(_ gesture: UISwipeGestureRecognizer) {
        //if let tabbar = self {
            if gesture.direction == .left && self.selectedIndex < 2 {
             self.selectedIndex += 1
            } else if gesture.direction == .right && self.selectedIndex > 0 {
                self.selectedIndex-=1
            }
       // }
    }
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        print("cambio!")
        //setNeedsStatusBarAppearanceUpdate()
        
        return true
    }
    func tabBarControllerPreferredInterfaceOrientationForPresentation(_ tabBarController: UITabBarController) -> UIInterfaceOrientation {
        print("cambio a portrait")
        return .portrait
    }
    func tabBarControllerSupportedInterfaceOrientations(_ tabBarController: UITabBarController) -> UIInterfaceOrientationMask {
        print("cambio a aal")
        return .all
    }
    
}
